
FeedAPI Imagegrabber
---------------------

The FeedAPI Imagegrabber module is an add-on module for FeedAPI. The  FeedAPI
ImageGrabber module visits the original URL of a new feed item, and retrieves
the main image from the posting. Once the main image has been retrieved, it is
then converted into a thumbnail using the ImageCache module, and stored in the
node created by FeedAPI (FeedAPI Node Processor), inside an ImageField CCK field.

Requirements
-------------

This module requires the following modules:

1) FeedAPI Node
2) CCK
3) Imagefield
4) ImageAPI
5) Views (Required by FeedAPI to show the feed items.)

To get things working you must enable the FeedAPI node processor which creates
nodes, in which an image will be inserted.

You must get the UrlToAbsolute Project from "http://sourceforge.net/projects/absoluteurl" and copy the script to the module directory.

Important: To make the module working, you must install php-curl properly. Look
           at www.php.net/curl on how to install curl on php.

Installation
-------------

1) Go to admin/build/modules and activate the module.
2) Download the UrltoAbsolute project from "http://sourceforge.net/projects/absoluteurl", unzip it and copy the PHP script(url_to_absolute.php) into the module folder. 
3) You will see an extra option for enabling the Imagegrabber while creating a
   feed.
4) Enable the Imagegrabber for a feed, select the imagefield for the feed-item
   content type.
5) Refresh the feed, if you want the images. (of course, you want it.)

Note: You have to add the CCK Imagefields manually to the content type of the
      feed-item.

For eg. : Say the feed creates nodes of content type story, then you must add an
          imagefield to the content type story to get going.

Author/credits
-----------

1) The author of the module is Nitin Kumar Gupta (http://drupal.org/user/472412).
2) I got lots of useful responses from my mentor Sidharth Kshatriya.

--
This project is sponsored by Srijan Technologies Pvt. Ltd.(www.srijan.in)

