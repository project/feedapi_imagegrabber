<?php

/**
 * @file
 * Grabs the primary image from the feed-item and
 * stores into a CCK imagefield
 */

/**
 * Implementation of hook_menu().
 */

function feedapi_imagegrabber_menu() {
    $items = array();
    $items['admin/settings/imagegrabber'] = array(
    'title' => 'ImageGrabber settings',
    'page callback' => 'feedapi_imagegrabber_admin_settings',
    'access callback'   => 'user_access',
    'access arguments'  => array('administer imagegrabber'), );

    return $items;
}

/**
 * Implementation of hook_perm().
 */

function feedapi_imagegrabber_perm() {
    return array('administer imagegrabber', 'enable imagegrabber');
}

/**
 * validates and uploads the image file into the cck image field
 *
 * @param   $filename   path to the image to be uploaded
 * @param   $field      cck image field instance to insert into which image is
 *                      uploaded (check before passing that this field exists)
 *
 * @return
 * fid of the image if the image is uploaded successfuly, 
 * FALSE if any error occurs.
 */

function feedapi_imagegrabber_image_upload($filename, $field, $replace = FILE_EXISTS_RENAME) {

    if (empty($filename)) {
        return 0;
    }

    $field_instance = content_fields($field, $node->type);

    $dest = filefield_widget_file_path($field_instance);

    if (!field_file_check_directory($dest, FILE_CREATE_DIRECTORY)) {
        form_set_error($upload_name, t('The file could not be uploaded.'));
        return 0;
    }

    global $user;

    $validators1 = imagefield_widget_upload_validators($field_instance);
    $validators2 = filefield_widget_upload_validators($field_instance);

    $validators = array_merge($validators1, $validators2);

    // Add in our check of the the file name length.
    $validators['file_validate_name_length'] = array();


    $extensions = '';
    foreach ($user->roles as $rid => $name) {
        $extensions .= ' '. variable_get("upload_extensions_$rid",
            variable_get('upload_extensions_default', 'jpg jpeg gif png txt html doc xls pdf ppt pps odt ods odp'));
    }

    // Begin building file object.
    $file = new stdClass();
    $file->filename = file_munge_filename(trim(basename($filename)), $extensions);
    $file->filepath = $filename;
    $file->filemime = file_get_mimetype($file->filename);


    // Rename potentially executable files, to help prevent exploits.
    if (preg_match('/\.(php|pl|py|cgi|asp|js)$/i', $file->filename) && (substr($file->filename, -4) != '.txt')) {
        $file->filemime = 'text/plain';
        $file->filepath .= '.txt';
        $file->filename .= '.txt';
    }

    // If the destination is not provided, or is not writable, then use the
    // temporary directory.
    if (empty($dest) || file_check_path($dest) === FALSE) {
        $dest = file_directory_temp();
    }

    $file->source = $field;

    $file->destination = $pathname =  file_destination(file_create_path($dest .'/'. $file->filename), $replace);

    if (!($file_temp = file_get_contents($filename))) {
        form_set_error(t('Error while copying  from the temp directory'));
        return 0;
    }
    if (!($pathname = file_save_data($file_temp, $pathname, FILE_EXISTS_RENAME))) {
        form_set_error(t('Error while pasting to the images folder of drupal'));
        return 0;
    }
    $file->filepath = $file->destination;
    chmod($pathname, 0744);
    if (!($filesize = filesize($filename))) {
        form_set_error(t('File size could not be calculated'));
        return 0;
    }

    $file->filesize = $filesize;

    // Call the validation functions.
    $errors = array();
    foreach ($validators as $function => $args) {
        array_unshift($args, $file);
        $errors = array_merge($errors, call_user_func_array($function, $args));
    }

    // Check for validation errors.
    if (!empty($errors)) {
        $message = t('The selected file %name could not be uploaded.', array('%name' => $file->filename));
        if (count($errors) > 1) {
            $message .= '<ul><li>'. implode('</li><li>', $errors) .'</li></ul>';
        }
        else {
            $message .= ' '. array_pop($errors);
        }
        form_set_error($source, $message);
        return 0;
    }

    $file->uid = $user->uid;
    $file->status = FILE_STATUS_TEMPORARY;
    $file->timestamp = time();

    drupal_write_record('files', $file);


    // Let modules add additional properties to the yet barebone file object.
    foreach (module_implements('file_insert') as $module) {
        $function =  $module .'_file_insert';
        $function($file);
    }


    $fid = $file->fid;
    return $fid;
}


/**
 * Implementation of hook_form_alter().
 */

function feedapi_imagegrabber_form_alter(&$form, $form_state, $form_id) {
    // Content type form.
    if ($form_id == 'node_type_form' && isset($form['identity']['type'])) {
        $form['#submit'][] = 'feedapi_imagegrabber_content_type_submit';
    }


    //// FeedAPI-enabled node form.
    if (user_access('enable imagegrabber')) {
        if (isset($form['type']) && isset($form['#node']) && $form['type']['#value'] .'_node_form' == $form_id && feedapi_enabled_type($form['type']['#value']) ) {


            //enable Imagegrabber only when a feed is editted.
            //if (!isset($form['#node']->nid))return;

            $node = $form['#node'];
            $settings = feedapi_get_settings($node->type);
            if (isset($settings['processors']['feedapi_node']['enabled']))
            $is_feedapi_node_enabled = $settings['processors']['feedapi_node']['enabled'];
            else $is_feedapi_node_enabled = FALSE;
            $form['imagegrabber'] = array(
          '#type' => 'fieldset',
          '#title' => t('Image Grabber'),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
          '#tree' => TRUE,
            );

            $form['imagegrabber']['value'] = array(
          '#type' => 'value',
          '#value' => $is_feedapi_node_enabled, );
            if (!$is_feedapi_node_enabled) {
                $form['imagegrabber']['message'] = array(
                '#title' => t('Enable the feedapi_node module first to use Imagegrabber'),
                    '#type' => 'item',
                    '#description' => t('ImageGrabber settings have been removed, because it needs the feedapi_node processor. Enable the feedapi_node processor for this feed'));
            }

            else {
                $enabled = feedapi_imagegrabber_is_enabled($node->nid);
                if ($enabled==1 || $enabled =='1') {
                    $image_field = feedapi_imagegrabber_image_field($node->nid);
                }

                $form['imagegrabber']['enabled'] = array(
                    '#type' => 'checkbox',
                    '#title' => t('Enable Image-Grabber for this feed'),
                    '#description' => t('Check if you want to use this feed downloading images of feed items to your site.'),
                    '#default_value' => $enabled,
                    '#weight' => -15,
                );

                $options = array(t('Select an Image field'), );
                //$add = content_fields();
                $settings = feedapi_get_settings($node->type, $node->nid);
                //@Todo make it a generic one and not only for the feedapi_node processor.
                $content_type_processor = $settings['processors']['feedapi_node']['content_type'];

                $info = _content_type_info();
                $add = $info['content types'][$content_type_processor]['fields'];
                foreach ($add as $type => $args) {

                    if ($args['type']=='image') {
                        $imagefield = $args['field_name'];
                        $options[$imagefield] =  $args['field_name'];
                    }
                }

                if ($enabled == '0' || $enabled == 0) {
                   $default = $options[0];
                }
                else {
                    $default = $image_field;
                }

                $form['imagegrabber']['image_field'] = array(
                '#type' => 'select',
                '#title' => t('Select a CCK Imagefield'),
                '#description' => t('The module stores the image into this image field of the feed item.The content type for the feed-item of this feed is @node', array('@node' => $content_type_processor)),
                '#options' => $options,
                '#default_value' => t($default)
                );

                if (count($options) ==1) {
                    $form['imagegrabber']['no_field'] = array(
                '#title' => t('ADD CCK Imagefields'),
                '#type' => 'item',
                '#description' => t('Add CCK image fields to the content type @node', array('@node' => $content_type_processor))
                    );
                }
            }
            //$form['#submit'][] = 'feedapi_imagegrabber_form_node_submit';
            $form['#validate'][] = 'feedapi_imagegrabber_form_node_validate';


        }
    }
}

/**
 * Implementation of hook_feedapi_after_refresh().
 *
 * Called after feed is refreshed. It downloads the images
 * and stores it into the cck image field.
 *
 * @param $feed     the feed object which contains all the feed items.
 *
 * @todo option to prevent look for images when an feed item is updated.
 * only on new addition. (save bandwidth, increase speed.).
 *
 */

function feedapi_imagegrabber_feedapi_after_refresh($feed) {

    if(!function_exists('curl_exec')) {
        drupal_set_message(t('Imagegrabber: The cURL Library for PHP is missing or outdated. Go to <a href="!admin-reports-status">Status Report page</a>', array('!admin-reports-status' => url('admin/reports/status'))),'error');
        return;
    }
    $path = drupal_get_path('module', 'feedapi_imagegrabber') .'/url_to_absolute.php';
    if(!file_exists($path)) {
    drupal_set_message(t('Imagegrabber: The Url Conversion script is missing. Go to <a href="!admin-reports-status">Status Report page</a>', array('!admin-reports-status' => url('admin/reports/status'))),'error');
        return;    
    }
    require_once($path);
    $feed = (array)$feed;
    if (!isset($feed['nid'])) return;
    $feed_nid = $feed['nid'];

    $enabled = feedapi_imagegrabber_is_enabled($feed_nid);
    $image_field = feedapi_imagegrabber_image_field($feed_nid);

    if (!isset($enabled) || $enabled == 0 || $enabled == '0') {
        return ;
    }

    if ($enabled == 1 && (!isset($image_field) || empty($image_field) || $image_field == '0')) {
        return;
    }

    $info = _content_type_info();

    foreach ($feed['items'] as $item => $argument) {
        if (isset($argument->nid)) {
            $nid = $argument->nid;
        }
        else {
            $original_url = $argument->options->original_url;
            if(!isset($original_url) || empty($original_url)) continue;
            $nid = db_fetch_object(db_query("SELECT nid FROM {feedapi_node_item} WHERE url = '%s'", $original_url));
            if(!isset($nid) || empty($nid)) continue;
        }
        if (!($node = node_load($nid))) continue;
        $fields = $info['content types'][$node->type]['fields'];
        if (!(array_key_exists($image_field, $fields))) continue;
        $original_url = $argument->options->original_url;
        $filename = feedapi_imagegrabber_select_image($original_url);
        if (!$filename) continue;
        if (!($fid = feedapi_imagegrabber_image_upload($filename, $image_field))) continue;
        if (isset($fid)) {
            $node->$image_field = array(array('fid' => $fid, 'list' => 1, ), );
            node_save($node);
        }

    }

}

/**
 *  checks whether the Image grabber is enabled for this node or not.
 *
 *  @param $nid     node to detect
 *
 *  @return
 *          TRUE if Image grabber enabled else FALSE
 */

function feedapi_imagegrabber_is_enabled($nid) {
    $enabled = db_result(db_query("SELECT enabled FROM {feedapi_imagegrabber} WHERE nid = %d", $nid));
    return $enabled;
}

/**
 *  returns the image field of the feed-item content type in which
 * ImageGrabber stores the image for the feed-item.
 *
 *  @param $nid node to select
 *
 *  @return
 *          the image field or else FALSE
 */

function feedapi_imagegrabber_image_field($nid) {

    $image_field = db_result(db_query("SELECT image_field FROM {feedapi_imagegrabber} WHERE nid = %d", $nid));
    return $image_field;

}


/**
 * validates the form altered for the feedapi enabled content type.
 */

function feedapi_imagegrabber_form_node_validate($form, &$form_state) {
    $value = $form_state['values']['imagegrabber']['value'];
    if ($value == 1) {
        $enabled = $form['imagegrabber']['enabled']['#value'];
        $image_field = $form['imagegrabber']['image_field']['#value'];

        if ($enabled == 1 && $image_field == '0') {
            form_set_error('image_field', "ImageGrabber is enabled but no image field is selected");
            return;
        }
    }
}

/*
 *  Implementation of hook_nodeapi().
 */

  function feedapi_imagegrabber_nodeapi(&$node, $op, $teaser, $page)
  {
      if(feedapi_enabled_type($node->type)) {
          switch($op) {
              case 'insert':
              case 'update':
                  $value = $node->imagegrabber['value'];
                  $nid = $node->nid;
                  if($value && isset($nid)) {
                      $enabled = $node->imagegrabber['enabled'];
                      if($enabled == 1) {
                        $image_field = $node->imagegrabber['image_field'];
                      }
                      else {
                        $image_field = '0';
                      }
                      db_query("UPDATE {feedapi_imagegrabber} SET enabled = %d , image_field = '%s' WHERE nid = %d", $enabled, $image_field, $nid);
                      if (!db_affected_rows() && $enabled == 1) {
                        @db_query("INSERT INTO {feedapi_imagegrabber} (nid, enabled, image_field) VALUES (%d, %d,'%s')", $nid, $enabled, $image_field);
                      }

                  }
                  break;

              case 'delete':
                  $nid = $node->nid;
                  @db_query("DELETE FROM {feedapi_imagegrabber} where nid = %d", $nid);
                  break;
          }
      }
  }

/**
 *  Form Builder. Configure ImageGrabber
 */

function feedapi_imagegrabber_admin_settings() {

    return t(('Coming soon.... We will keep the help content to use the module here'));

}


/**
 * checks if the feedapi_node processor is about to be disabled.
 */
function feedapi_imagegrabber_content_type_submit($form, &$form_state) {
    if (isset($form_state['values']['feedapi']['processors']['feedapi_node']) && !$form_state['values']['feedapi']['processors']['feedapi_node']['enabled']) {
        if (module_exists('feedapi_imagegrabber')) {
            drupal_set_message(t('ImageGrabber is disabled for the content type !type as it depends on feedapi_node processor.', array('!type' => $form_state['values']['name'])), 'warning');
            return;
        }
    }
}



function feedapi_imagegrabber_select_image($original_url) {    
    $image_file = '';
    $image_found = 0;
    $selected_file = '';
    $base = parse_url($original_url, PHP_URL_HOST);
    $rest = $file = file_get_contents($original_url, FILE_TEXT);
    $count = 0;
    $doc = new DOMDocument();
    @$doc->loadHTML($rest);
    $xpath = new DOMXPath($doc);
    $hrefs = $xpath->evaluate("/html/body//img");
    $temp_dir = file_directory_temp();
    $size_image_found = 0;
    for ($i = 0; $i < $hrefs->length; $i++) {
        $href = $hrefs->item($i);
        $url = $href->getAttribute('src');

        $abs = url_to_absolute($base, $url);
        if (abs!=FALSE) {
            $image_file = $temp_dir .'/'. basename($url);
            if (feedapi_imagegrabber_download_image_heuristics($url, $image_file, $size_image_found)) {
                $image_found =1;
                $selected_file = $image_file;
            }
        }
    }
    if ($image_found) return $selected_file;
    else return FALSE;
}

/**
 * Heuristics for selecting the right image from the original feed item
 * of the URL
 * 1) select the image greater than some minimum resolution.
 * 2) select the largest amongst the images found.
 * @param $image_url	url of the image.
 * @param $filename     path of the file download image into.
 *
 * @return  boolean
 *          TRUE if image was selected.
 *          FALSE if rejected or any other error as timeout, invalid url, etc.
 *
 * @todo instead of downloading the image, try to search for height and width tag. preserve the bandwidth
 */

function feedapi_imagegrabber_download_image_heuristics($image_url, $filename, &$imagesize) {

    if (empty($filename)) return FALSE;
    //So many feed-items will be created, do not show any error messages if
    //fails
    if(!feedapi_imagegrabber_get_image($image_url,$filename)) return FALSE;

    $info = @getimagesize ($filename);
    $height = $info[0];
    $width = $info[1];

    $resolution = $height * $width ;

    if ($resolution < 22500 || $resolution < $imagesize) {
        unlink($filename);
        return FALSE;
    }
    $imagesize = $resolution;
    return TRUE;
}

/**
 * Downloads an image from the given URL and stores it into the given
 * filename.
 *
 * @param   $image_url  the url of the image
 * @param   $filename   the name of the file.
 *
 * @return boolean
 * true on sucess else false for any errors.
 */

function feedapi_imagegrabber_get_image($image_url, $filename)
{
    if(function_exists('curl_exec')) {
      $options = array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HEADER => false,
		CURLOPT_CONNECTTIMEOUT => 60,      // timeout on connect
		CURLOPT_TIMEOUT        => 60,      // timeout on response
	    );

    $fp = fopen($filename, "w");
    if(!$fp) return FALSE;

    $ch = curl_init($image_url);
	  curl_setopt_array($ch, $options);
	  curl_setopt($ch, CURLOPT_FILE, $fp);
    
    $content = curl_exec($ch);
    $err = curl_errno($ch);
    fclose($fp);
    curl_close($ch);
    if(!$err) return TRUE;
    else return FALSE;
    }
    else {
        return FALSE;
    }
}
